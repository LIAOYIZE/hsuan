//
//  Note.swift
//  hhh
//
//  Created by 林旻萱 on 2020/5/13.
//  Copyright © 2020 Hsuan. All rights reserved.
//

import Foundation
import UIKit


class Note: Equatable {
    static func == (lhs: Note, rhs: Note) -> Bool {
        return lhs === rhs
    }
    
    
    var dateText : String?
    var cityName : String?
    var noteText : String?
    var photo : UIImage?
    var noteID : String?
    
    init() {
        self.noteID = UUID().uuidString
    }
    
}

/*

 init(date: String, city: String) {
     self.date = date
     self.cityName = city
 }
    
 */
