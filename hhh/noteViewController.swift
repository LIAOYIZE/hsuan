//
//  noteViewController.swift
//  hhh
//
//  Created by 林旻萱 on 2020/5/13.
//  Copyright © 2020 Hsuan. All rights reserved.
//

import UIKit

protocol noteViewControllerDelegate {
    func didFinishUpdate(note: Note)
}

class noteViewController: UIViewController {

    var currentNote : Note!
    var delegate : noteViewControllerDelegate?
    
    @IBOutlet weak var dateTextView: UITextView!
    @IBOutlet weak var cityTextView: UITextView!
    @IBOutlet weak var noteTextView: UITextView!
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dateTextView.text = self.currentNote?.dateText
        self.cityTextView.text = self.currentNote?.cityName
        self.noteTextView.text = self.currentNote?.noteText
        self.imageView.image = self.currentNote?.photo
        
    }
    
    @IBAction func done(_ sender: Any) {
        
        self.currentNote.dateText = self.dateTextView.text
        self.currentNote.cityName = self.cityTextView.text
        self.currentNote.noteText = self.noteTextView.text
        
        self.delegate?.didFinishUpdate(note: self.currentNote)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
  

}
