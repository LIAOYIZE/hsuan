//
//  DispatchQueue+Once.swift
//  HelloMyMap
//
//  Created by 林旻萱 on 2020/4/23.
//  Copyright © 2020 Hsuan. All rights reserved.
//

import Foundation

extension DispatchQueue {
    
    //全域的Global
    private static var _onceTokens = [String]() //私有private,只有自己DispatchQueue可以用,靜態變數會被配一個記憶體 不會消失 直到app關閉(run過就不再run
    
    public class func once(token:String,job:()->Void) { //class類別方法public:整個專案的任何地方都可以用 token取什麼名字都可以,closure()->Void沒參數也沒吐東西
        
        //Thread Safe 需考量這個點,上鎖,避免同時執行到同個Thread crash
       objc_sync_enter(self) //上鎖 系統底層的API
        
        /*用guard else寫:guard !_onceTokens.contains(token) else{
            return
        }*/
        
        defer {  //延後、延遲。後面放的是closure,先保存起來,直到離開這段程式碼的範疇 會自動執行defer 若放到此段最後面會沒有用到(eg.從return出去 就沒有用了)
            objc_sync_exit(self)  //解鎖
        }
        
        //Check if it is exist?檢查arrays內是否存在token contains是檢查陣列是否存值:傳回true false
        if _onceTokens.contains(token){
           // objc_sync_exit(self) 解鎖 包進defer裡就不用每個出口都放這行
            return
        }
        
        _onceTokens.append(token)
        job()  //執行job
        //objc_sync_exit(self)
    }
}
