//
//  ListViewController.swift
//  hhh
//
//  Created by 林旻萱 on 2020/5/13.
//  Copyright © 2020 Hsuan. All rights reserved.
//

import UIKit

class ListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,addNoteViewControllerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var data : [Note] = []
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
    }
    
    
    @IBAction func addNote(_ sender: Any) { //看不到畫面 但還是要讓程式知道在陣列中要有地方可以放東西
        let note = Note()
//        note.dateText = " "
//        note.cityName = " "
        self.data.insert(note, at: 0)
     
        
        if let addVC = self.storyboard?.instantiateViewController(withIdentifier: "addVC") as? addNoteViewController {
            addVC.newNote = note
//            addVC.citytest = self.data[0].cityName
//            addVC.datetest = self.data[0].dateText
            addVC.delegate = self
            self.navigationController?.pushViewController(addVC, animated: true)
            
        }
        self.tableView.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listNote", for: indexPath)
        let note = self.data[indexPath.row]
        cell.textLabel?.text = note.dateText
        cell.detailTextLabel?.text = note.cityName
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "addNewSegue" {
//            let addNoteVC = segue.destination as! addNoteViewController
//
//        }else if segue.identifier == "noteSegue" {
//
//            let noteVC = segue.destination as! noteViewController
//            if let indexPath = self.tableView.indexPathForSelectedRow {
//                let note = self.data[indexPath.row]
//                noteVC.currentNote = note
//                noteVC.delegate = self
//            }
//        }
        
          if segue.identifier == "addNewSegue" {
                    let addNoteVC = segue.destination as! addNoteViewController
        
                }else if segue.identifier == "noteSegue" {
        
                    let noteVC = segue.destination as! noteViewController
                    if let indexPath = self.tableView.indexPathForSelectedRow {
                        let note = self.data[indexPath.row]
                        noteVC.currentNote = note
                        noteVC.delegate = self
                    }
                }
        
    }
    
    func didFinishNewone(note: Note) {
         
              //這一行不需要了,因為不用判斷位置,都是第0筆
//              if let position = self.data.firstIndex(of: note){
                let indexPath = IndexPath(row: 0, section: 0)
                  self.tableView.reloadRows(at: [indexPath], with: .automatic)
                  
//              }
          }
}

extension ListViewController: noteViewControllerDelegate {
    func didFinishUpdate(note: Note) {
        if let position = self.data.firstIndex(of: note){
            let indexPath = IndexPath(row: position, section: 0)
            self.tableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }
    
    
}
