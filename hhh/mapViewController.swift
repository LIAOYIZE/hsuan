//
//  mapViewController.swift
//  hhh
//
//  Created by 林旻萱 on 2020/5/13.
//  Copyright © 2020 Hsuan. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class mapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    let locationManger = CLLocationManager()
    
    let annotitionLocations = [
        ["title":"test1" , "latitude" : 25.036798, "longitude":121.499962],
        ["title":"test2" , "latitude": 25.063059, "longitude" :121.533838]
    ]
    
    @IBOutlet weak var mainMapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()

        mainMapView.delegate = self
        guard CLLocationManager.locationServicesEnabled() else {
                  //show some hint to user (user有沒有啟用定位服務) locationServicesEnabled全域的
                  return
              }
        mainMapView.mapType = .standard
        mainMapView.isZoomEnabled = true // 允許縮放地圖
        
        locationManger.requestLocation()
        locationManger.desiredAccuracy = kCLLocationAccuracyBest
        locationManger.activityType = .other
        locationManger.delegate = self
        locationManger.startUpdatingLocation()
        
        
        
        // 地圖預設顯示的範圍大小 (數字越小越精確)
//        let latDelta = 0.05
//        let longDelta = 0.05
//        let currentLocationSpan:MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: latDelta, longitudeDelta: longDelta)
//      let center:CLLocation = CLLocation(
//        latitude: 25.05, longitude: 121.515)
//      let currentRegion:MKCoordinateRegion = MKCoordinateRegion(center: center.coordinate,
//          span: currentLocationSpan)
//      mainMapView.setRegion(currentRegion, animated: true)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //let currentLocation :CLLocation = locations[0] as CLLocation
        guard let coordinate = locations.last?.coordinate else {
            return
        }
        
        print("Coordinate: \(coordinate.latitude), \(coordinate.longitude)")
        
        DispatchQueue.once(token: "addAnnotation") { //DispatchQueue.once使用方式addAnnotation同個名字(可隨便取)的工作只執行一次
            addAnnotation(coordinate: coordinate) //新增圖標(+大頭針)
        }
        
        DispatchQueue.once(token: "moveRegion"){
            moveRegion(coordinate: coordinate)
        }
        
        
    }
    
    func addAnnotation(coordinate:CLLocationCoordinate2D) {
        // 建立一個地點圖示 (圖示預設為紅色大頭針)
        var testAnnotation = MKPointAnnotation()
        testAnnotation.coordinate = CLLocation(
            latitude: 25.036798, longitude:121.499962).coordinate
        testAnnotation.title = "艋舺公園"
        testAnnotation.subtitle = "公園"
        mainMapView.addAnnotation(testAnnotation)
        
        testAnnotation = MKPointAnnotation()
        testAnnotation.coordinate = CLLocation(
            latitude: 25.063059,longitude: 121.533838).coordinate
        testAnnotation.title = "行天宮"
        testAnnotation.subtitle = "廟宇"
        mainMapView.addAnnotation(testAnnotation)
    }
    
    //一進去指針就在你設定的所在位置 地圖縮放(不用自已一直放大到點的位置)
    func moveRegion(coordinate:CLLocationCoordinate2D) {
        let span = MKCoordinateSpan (latitudeDelta: 0.001, longitudeDelta: 0.001)//上下緣差0.001緯度、左右緣差0.001經度 數值越大,範圍越大 數值設一樣 apple會自動經度配合緯度調整
        let region = MKCoordinateRegion(center: coordinate, span: span) //region縮放地圖 組成：center 地圖中心在哪
        mainMapView.setRegion(region, animated: true) //region移動到我要的地方 帶動話
    }
    
//    func addAnnotation(coordinate:CLLocationCoordinate2D) {
//        let annotation = MKPointAnnotation()
//        annotation.title = "test"
//        annotation.subtitle = "wow"
//        mainMapView.addAnnotation(annotation)
//    }

    //自定義大頭針樣式
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation)-> MKAnnotationView? {
        if annotation is MKUserLocation {
            // 建立可重複使用的 MKAnnotationView
            let reuseId = "MyPin"
            var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
            if pinView == nil {
                // 建立一個地圖圖示視圖
                pinView = MKAnnotationView(annotation: annotation,reuseIdentifier: reuseId)
                // 設置點擊地圖圖示後額外的視圖
                pinView?.canShowCallout = false
                // 設置自訂圖示
                pinView?.image = UIImage(named:"pointRed")
            } else {
                pinView?.annotation = annotation
            }
            
            return pinView
        } else {
            
            // 其中一個地點使用預設的圖示 這邊比對到座標時就使用預設樣式 不再額外設置
            if annotation.coordinate.latitude == 25.036798 && annotation.coordinate.longitude
                == 121.499962 {
                return nil
            }
            // 建立可重複使用的 MKPinAnnotationView
            let reuseId = "Pin"
            var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId) as? MKPinAnnotationView
            if pinView == nil {
                // 建一個大頭針視圖
                pinView = MKPinAnnotationView(annotation: annotation,reuseIdentifier: reuseId)
                pinView?.canShowCallout = true  // 設置點擊大頭針後額外的視圖
                pinView?.animatesDrop = true
                pinView?.pinTintColor = .blue
                // 這邊將額外視圖的右邊視圖設為一個按鈕
                pinView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            } else {
                pinView?.annotation = annotation
            }
            
            return pinView
        }
        
    }
    

}
