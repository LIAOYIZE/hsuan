//
//  addNoteViewController.swift
//  hhh
//
//  Created by 林旻萱 on 2020/5/14.
//  Copyright © 2020 Hsuan. All rights reserved.
//

protocol addNoteViewControllerDelegate {
    func didFinishNewone(note: Note)
}

import UIKit
import IQKeyboardManagerSwift
import TLPhotoPicker

class addNoteViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIPickerViewDataSource,UIPickerViewDelegate,TLPhotosPickerViewControllerDelegate {
 

    var newNote: Note!
    var image :[UIImage?] = []
    var selectedAssets = [TLPHAsset]() //TLPHAsset如何轉成UIImage
    var delegate : addNoteViewControllerDelegate?
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var cityTextField: UITextField!
    
    @IBOutlet weak var cityNote: UITextField!
    @IBOutlet weak var photoLabel: UILabel!
    @IBOutlet weak var showPhotoView: UICollectionView!
    
    
    var myCityName = ["請選擇","台北市","新北市","基隆市","桃園市","宜蘭市"]
    let formatter: DateFormatter = DateFormatter()
    var citytest : String?
    var datetest : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
         //formatter = DateFormatter()
        print("addVC:\(citytest)")
        formatter.dateFormat = "yyyy/MM/dd"
      
         dateLabel.text = "日期"
         cityLabel.text = "城市"
         photoLabel.text = "照片"
        
         
        let datepicker = UIDatePicker()
        datepicker.datePickerMode = .date
        datepicker.addTarget(self, action: #selector(addNoteViewController.datePickerChanged(datePicker:)), for: .valueChanged)
        dateTextField.inputView = datepicker
        dateTextField.tag = 100 // 設 UITextField tag 以利後續使用
        
        let citypicker = UIPickerView()
        citypicker.dataSource = self
        citypicker.delegate = self
        cityTextField.inputView = citypicker
        cityTextField.text = myCityName[0]
        cityTextField.tag = 200
        
        self.showPhotoView.dataSource = self
        self.showPhotoView.delegate = self
        let screenSize = UIScreen.main.bounds.size // 取得螢幕的尺寸
        let layout = UICollectionViewFlowLayout ()
        layout.sectionInset = UIEdgeInsets(top: 0.3, left: 0.3, bottom: 0.3, right: 0.3) //section間距
        layout.minimumLineSpacing = 0.5 //每一行的間距
        let side = screenSize .width / 3 - 1  //因為前後都空0.5所以要減1
        layout.itemSize = CGSize(width: side, height: side)
        self.view.addSubview(showPhotoView)
        
 
    }
    
    // UIDatePicker 改變選擇時執行的動作
    @objc func datePickerChanged(datePicker:UIDatePicker) {
        // 依據元件的 tag 取得 UITextField。 要實作addTarget
        let chooseDateTextField = self.view?.viewWithTag(100) as? UITextField
        // 將 UITextField 的值更新為新的日期。 要建立formatter
        chooseDateTextField?.text = formatter.string(from: datePicker.date)
        self.datetest = chooseDateTextField?.text
        self.newNote?.dateText = chooseDateTextField!.text
        
        
    }

    @IBAction func addPhoto(_ sender: Any) {
        
        let photoViewController = TLPhotosPickerViewController()
        photoViewController.delegate = self
        self.present(photoViewController, animated: true, completion: nil)
        
    }
 
    @IBAction func save(_ sender: Any) {
 
        self.newNote?.dateText = self.dateTextField.text
        self.newNote?.cityName = self.cityTextField.text
        self.delegate?.didFinishNewone(note: self.newNote)
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    //MARK: -UIPickerViewDataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return myCityName.count
    }
    
    // UIPickerView 每個選項顯示的資料
    func pickerView(_ pickerView: UIPickerView,titleForRow row: Int,forComponent component: Int) -> String? {
        // 設置為陣列 cityName 的第 row 項資料
        return myCityName[row]
    }
    
    // UIPickerView 改變選擇後執行的動作
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        // 依據元件的 tag 取得 UITextField
        let chooseCityTextField = self.view?.viewWithTag(200) as? UITextField
        // 將 UITextField 的值更新為陣列 cityName 的第 row 項資料
        chooseCityTextField?.text = myCityName[row]
//        print(chooseCityTextField?.text)
//        self.citytest = chooseCityTextField?.text
//        self.newNote?.cityName = chooseCityTextField!.text
    }
    
    //MARK: -TLPhotosPickerViewControllerDelegate
    func dismissPhotoPicker(withTLPHAssets: [TLPHAsset]) {
        //使用者選好照片時
        self.selectedAssets = withTLPHAssets
        //self.image = self.selectedAssets
        getFirstSelectedImage()
        showPhotoView.reloadData()
    }
    func getFirstSelectedImage() {

        for i in self.selectedAssets {
            if let image = i.fullResolutionImage {
                self.image.append(image)
            }
        }
    }
        func photoPickerDidCancel() {
            //取消選取照片
        }
        func dismissComplete() {
            //完成照片選取並離開
        }
        func didExceedMaximumNumberOfSelection(picker: TLPhotosPickerViewController) {
            //選取超過最大上限數量的照片
        }
}

//MARK: CollectionViewDelegate
extension addNoteViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return image.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! myImageCell
        cell.myImage.image = image[indexPath.item]
        return cell
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        1
    }
}
